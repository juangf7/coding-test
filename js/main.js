// display terms or faqs

const terms = document.querySelector("#chck1");
const faqs = document.querySelector("#chck2");
let terms_check = true;
let faqs_check = false;

function toggleDiv() {
  terms_check = !terms_check;
  terms.checked = terms_check;
  faqs_check = !faqs_check;
  faqs.checked = faqs_check;
}

function showFaqs() {
  terms.checked = false;
  faqs.checked = true;
}

terms.onclick = toggleDiv;
faqs.onclick = toggleDiv;

// show faqs on click
let faqs_divs = document.querySelectorAll(".item");

function toggleQuestion() {
  this.children[0].classList.toggle("minus");
  this.children[1].classList.toggle("hidden");
}

function openQuestion(questionNum) {
  let faq_target = document.querySelector("#q" + questionNum);
  faq_target.children[0].classList.toggle("minus");
  faq_target.children[1].classList.toggle("hidden");
}

for (let item of faqs_divs) {
  item.addEventListener("click", toggleQuestion);
}

// Go to specific question
function scrollTo(hash) {
  location.hash = "#q" + hash;
}

const questions = /#q/;
let url = window.location.href;

if (url.match(questions)) {
  showFaqs();
  let questionNum = url.split("#q")[1];
  openQuestion(questionNum);
  scrollTo(questionNum);
}
